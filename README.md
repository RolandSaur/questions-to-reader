# Questions to Reader
Guiding a reader to the point on which you want feedback on. 
Does so in two ways.
By adding questions in the text using [mdframed](https://www.ctan.org/pkg/mdframed?lang=en)

![q2r](./media/q2r.png)

and by creating a table of questions at the beginning of the file.

![q2r_toc](./media/q2r_toc.png)

*Note:* Because the table of questions relies on an `.aux` file you need to run your latex command twice.

# TODO:
- [ ] Group list of Questions by sections.
